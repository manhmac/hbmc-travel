// SLIDE INDEX
$('.slide-id').slick({
  dots: true,
  infinite: true,
  speed: 500,
  fade: true,
  cssEase: 'linear',
  autoplay: true,
  autoplaySpeed: 4000,
  height:800,
});
// .

// SILDE KHÁCH SẠN
$('.slide-list-place').slick({
  centerMode: true,
  slidesToShow: 1,
   centerPadding: '25%',
   dots: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: false,
        slidesToShow: 1,
        centerPadding: '25%',
      }
    },
    {
      breakpoint: 480,
      settings: {

        arrows: false,
        centerMode: false,
        slidesToShow: 1
      }
    }
  ]
});
// .



// slick-box-small-img-detail tour
$('.pdl-small-images').slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  dots: false,
  centerMode: false,
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: false,
        slidesToShow: 3,
        centerPadding: '25%',
      }
    },
    {
      breakpoint: 480,
      settings: {

        arrows: false,
        centerMode: false,
        slidesToShow: 3
      }
    }
  ]

});


// tabs
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Back to top
  $(".back-to-top a").click(function (n) {
      n.preventDefault();
      $("html, body").animate({
          scrollTop: 0
      }, 500)
  });
  $(window).scroll(function () {
      $(document).scrollTop() > 1e3 ? $(".back-to-top").addClass("display") : $(".back-to-top").removeClass("display")
  });


